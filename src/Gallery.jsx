import React,{Component} from 'react';
import './App.css';

class Gallery extends Component  {

  constructor(props){
    super(props);

    this.state ={
      playingUrl: '',
      audio: null,
      playing:false
    }
  }

  playAudio(prewUrl){
    let audio = new Audio(prewUrl);
    if(!this.state.playing){
      audio.play();
      this.setState({playing: true, playingUrl: prewUrl,audio});
    } else{
      if(this.state.playingUrl === prewUrl){
        this.state.audio.pause();
        this.setState({playing: false});
      }else{
        this.state.audio.pause();
        audio.play();
        this.setState({playing: true, playingUrl: prewUrl,audio});
      }
    }
  }

  render(){ 
    console.log('Gallery.props',this.props);
    const { tracks } = this.props;
    return(
      <div className="Gallery">
        {tracks.map((track,k)=>{
          const trackImg = track.album.images[0].url;
          return(
            <div 
              key={k} 
              onClick={()=> this.playAudio(track.preview_url)}
              className="track">
              <img src={trackImg} className="track-img" alt="track" />
              <div className='track-play'>
                <div className='track-play-inner'>
                <span class={this.state.playingUrl === track.preview_url && this.state.playing 
                              ? 'glyphicon glyphicon-pause' :'glyphicon glyphicon-play'}></span>
                </div>
              </div>
              <p className="track-text">{track.name}</p>

            </div>
          )
        })}
      </div>
    )
  }
}

export default Gallery;
